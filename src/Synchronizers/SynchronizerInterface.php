<?php namespace OrderSynchronizer\Synchronizers;

use Shopware\Core\Checkout\Order\OrderCollection;
use Shopware\Core\Checkout\Order\OrderEntity;

interface SynchronizerInterface{

    /**
     * Sends the orders to the defined provider
     * @param OrderCollection $orderCollection
     */
    public function synchronize(OrderCollection $orderCollection) : void;

    /**
     * Syncs order payment status change
     *
     * @param OrderEntity $order
     */
    public function updateOrderPaymentStatus(OrderEntity $order) : void;
}
