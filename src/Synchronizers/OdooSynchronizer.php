<?php namespace OrderSynchronizer\Synchronizers;

use Exception;
use GuzzleHttp\Client;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Shopware\Core\Checkout\Order\OrderCollection;
use Shopware\Core\Checkout\Order\OrderEntity;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\ItemInterface;

class OdooSynchronizer extends BaseSynchronizer{

    /** @var Client */
    protected Client $http;

    protected string $tenant;

    /**
     * OdooSynchronizer constructor.
     *
     * @param LoggerInterface $logger
     * @param iterable $transactionResolvers
     */
    public function __construct(LoggerInterface $logger, iterable $transactionResolvers)
    {
        parent::__construct($logger, $transactionResolvers);

        // Initialize guzzle client
        $this->http = new Client();

        $this->tenant = getenv('SHOPWARE_TENANT_ID');
    }

    /**
     * @param string|null $path
     *
     * @return string
     */
    protected function getApiUrl(string $path = null) : string
    {
        return getenv('ODOO_SHOPWARE_API_URL') . $path;
    }

    /**
     * @param OrderCollection $orderCollection
     *
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function synchronize(OrderCollection $orderCollection): void
    {
        $orderSyncStatuses = $this->getOrdersSyncStatus($orderCollection);
        foreach($orderSyncStatuses as $orderSyncStatus){
            if($orderSyncStatus->synchronized){ continue; }
            $this->synchronizeOrder($orderCollection->get($orderSyncStatus->orderId));
        }
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function synchronizeOrder(OrderEntity $order) : void
    {
        $this->logger->info('OdooSynchronizer create order for order id '. $order->getId());

        $this->logger->info('order data: ', [$order]);

        $mergedOrderData = array_merge(array($order), [
            'pspReference' => $this->getTransactionResolverForOrder($order)->getTransactionId($order)
        ]);

         // If order found, send to odoo
        $response = $this->http->post($this->getApiUrl("/$this->tenant/out/order"), [
            'headers' => $this->getAuthHeader(),
            'json' => $mergedOrderData,
        ]);

        $result = json_decode($response->getBody()->getContents());

        $this->logger->info('result: ', [$result]);

        // If no result or no success property from result
        if(! $result || ! property_exists($result, 'success')){
            throw new Exception("No result received from Odoo API. Order ID: {$order->getId()}");
        }

        // If there is success property, but its value is false
        if(! $result->success){
            throw new Exception("There was an error synchronizing order to Odoo API. Order ID: {$order->getId()}");
        }
    }

    /**
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function getOrdersSyncStatus(OrderCollection $orderCollection) : array
    {
        $response = $this->http->get($this->getApiUrl("/$this->tenant/out/order/sync-status"), [
            'headers' => $this->getAuthHeader(),
            'json' => ['orders' => $orderCollection->getIds()],
        ]);

        $result = json_decode($response->getBody()->getContents());

        if(! is_array($result) || ! count($result) || ! property_exists($result[0], "orderId")){
            throw new Exception("Could not check if orders were already synchronized.");
        }

        return $result;
    }

    /**
     * @inheritDoc
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function updateOrderPaymentStatus(OrderEntity $order) : void
    {
        $mergedOrderData = array_merge(array($order), [
            'pspReference' => $this->getTransactionResolverForOrder($order)->getTransactionId($order)
        ]);

        // If order found, send to odoo
        $response = $this->http->put($this->getApiUrl("/$this->tenant/out/order"), [
            'headers' => $this->getAuthHeader(),
            'json' => $mergedOrderData,
        ]);

        $result = json_decode($response->getBody()->getContents());

        // If no result or no success property from result
        if(! $result || ! property_exists($result, 'success')){
            throw new Exception("No result received from Odoo API. Order ID: {$order->getId()}");
        }

        // If there is success property but it's value is false
        if(! $result->success){
            throw new Exception("There was an error synchronizing order to Odoo API. Order ID: {$order->getId()}");
        }

        // If reaches here, all good
        return;
    }

    /**
     * @return string[]
     * @throws InvalidArgumentException
     */
    protected function getAuthHeader() : array{
        return ['Authorization' => "Bearer {$this->getAuthToken()}"];
    }

    /**
     * @return mixed
     * @throws InvalidArgumentException
     */
    protected function getAuthToken()
    {
        // Get cache adapter
        $cache = new FilesystemAdapter();

        // Get/set cache
        return $cache->get('odoo_shopware_api_auth_token', function(ItemInterface $item){

            // Request the token
            $response = $this->http->post($this->getApiUrl("/oauth/token"), [
                'form_params' => [
                    'grant_type'    => 'client_credentials',
                    'client_id'     => getenv('ODOO_SHOPWARE_API_CLIENT_ID'),
                    'client_secret' => getenv('ODOO_SHOPWARE_API_CLIENT_SECRET'),
                ],
            ]);

            // Get the contents
            $contents = json_decode($response->getBody()->getContents());

            if(! $contents || ! property_exists($contents, 'expires_in') || ! property_exists($contents, 'access_token')){
                return null;
            }

            // Remove 1 second from the expires in, just to be sure we don't have a call at the moment it expires and the token is invalid
            $item->expiresAfter($contents->expires_in - 1);

            // Return the token
            return $contents->access_token;
        });
    }
}
