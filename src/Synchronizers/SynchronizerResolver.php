<?php namespace OrderSynchronizer\Synchronizers;

use Exception;
use Shopware\Core\System\SystemConfig\SystemConfigService;

class SynchronizerResolver{

    /** @var \Shopware\Core\System\SystemConfig\SystemConfigService */
    protected SystemConfigService $systemConfigService;

    /** @var iterable */
    protected iterable $synchronizers;

    public function __construct(SystemConfigService $systemConfigService, iterable $synchronizers)
    {
        $this->systemConfigService = $systemConfigService;
        $this->synchronizers = $synchronizers;
    }

    /**
     * @return SynchronizerInterface
     * @throws Exception
     */
    public function getSynchronizerDriverByName() : SynchronizerInterface
    {
        // Prepare driver class
        $driverName = $this->systemConfigService->get('OrderSynchronizer.config.driver');
        $driverClass = ucfirst($driverName) . 'Synchronizer';
        $driverClass = "OrderSynchronizer\\Synchronizers\\$driverClass";

        foreach($this->synchronizers as $synchronizer){

            if(get_class($synchronizer) == $driverClass){

                if(! is_subclass_of($driverClass, SynchronizerInterface::class)){
                    throw new Exception("The driver $driverClass does not implement SynchronizerInterface");
                }

                return $synchronizer;
            }
        }

        throw new Exception("There is no driver $driverClass mapped.");
    }
}