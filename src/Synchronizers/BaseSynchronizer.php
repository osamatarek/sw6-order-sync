<?php namespace OrderSynchronizer\Synchronizers;

use OrderSynchronizer\Services\TransactionResolverInterface;
use Psr\Log\LoggerInterface;
use Shopware\Core\Checkout\Order\Aggregate\OrderTransaction\OrderTransactionEntity;
use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;

abstract class BaseSynchronizer implements SynchronizerInterface{

    // Used for shared functionality between synchronizes

    /** @var LoggerInterface */
    protected LoggerInterface $logger;

    /** @var iterable */
    protected iterable $transactionResolvers;

    /**
     * BaseSynchronizer constructor.
     *
     * @param LoggerInterface $logger
     * @param iterable $transactionResolvers
     */
    public function __construct(LoggerInterface $logger, iterable $transactionResolvers)
    {
        $this->logger = $logger;
        $this->transactionResolvers = $transactionResolvers;
    }

    /**
     * @param OrderEntity $orderEntity
     *
     * @return TransactionResolverInterface
     * @throws \Exception
     */
    protected function getTransactionResolverForOrder(OrderEntity $orderEntity): TransactionResolverInterface
    {
        /** @var OrderTransactionEntity $latestTransaction */
        // Todo: check if this is the most recent transaction
        $latestTransaction = $orderEntity->getTransactions()->last();

        return $this->findTransactionResolverByPaymentMethodHandlerIdentifier(
            $latestTransaction->getPaymentMethod()->getHandlerIdentifier()
        );
    }

    /**
     * @param string $identifier
     *
     * @return TransactionResolverInterface
     * @throws \Exception
     */
    protected function findTransactionResolverByPaymentMethodHandlerIdentifier(string $identifier) : TransactionResolverInterface
    {
        /** @var TransactionResolverInterface $transactionResolver */
        foreach($this->transactionResolvers as $transactionResolver){
            if(str_contains($identifier, $transactionResolver->getPluginKey())){
                return $transactionResolver;
            }
        }
        throw new \Exception("Identifier $identifier has no resolver mapped.");
    }
}
