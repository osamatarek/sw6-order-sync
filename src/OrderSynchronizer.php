<?php declare(strict_types=1);

namespace OrderSynchronizer;

use OrderSynchronizer\DataLoaders\OrderDataLoader;
use OrderSynchronizer\Synchronizers\OdooSynchronizer;
use OrderSynchronizer\Synchronizers\SynchronizerInterface;
use Shopware\Core\Framework\Plugin;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OrderSynchronizer extends Plugin
{
}
