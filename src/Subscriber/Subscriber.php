<?php declare(strict_types=1);

namespace OrderSynchronizer\Subscriber;

use Exception;
use OrderSynchronizer\DataLoaders\OrderDataLoader;
use OrderSynchronizer\Synchronizers\SynchronizerResolver;
use Psr\Log\LoggerInterface;
use Shopware\Core\Checkout\Order\Event\OrderStateMachineStateChangeEvent;
use Shopware\Core\Checkout\Order\OrderCollection;
use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\Checkout\Order\OrderEvents;
use Shopware\Core\Framework\DataAbstractionLayer\EntityWriteResult;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityWrittenEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class Subscriber implements EventSubscriberInterface
{

    /*** @var LoggerInterface  */
    protected LoggerInterface $logger;

    protected SynchronizerResolver $synchronizerResolver;

    protected OrderDataLoader $orderDataLoader;


    /**
     * Subscriber constructor.
     *
     * @param LoggerInterface $logger
     * @param \OrderSynchronizer\Synchronizers\SynchronizerResolver $synchronizerResolver
     * @param \OrderSynchronizer\DataLoaders\OrderDataLoader $orderDataLoader
     */
    public function __construct(LoggerInterface $logger, SynchronizerResolver $synchronizerResolver, OrderDataLoader $orderDataLoader)
    {
        // Assign parameters
        $this->logger = $logger;
        $this->synchronizerResolver = $synchronizerResolver;
        $this->orderDataLoader = $orderDataLoader;
    }

    public static function getSubscribedEvents(): array
    {
        // Return the events to listen to as array like this:  <event to listen to> => <method to execute>
        return [
            OrderEvents::ORDER_WRITTEN_EVENT            => 'onOrderWrittenEvent',
            'state_enter.order_transaction.state.paid'  => 'onOrderStateChange',
        ];
    }

    /**
     * @param EntityWrittenEvent $event
     * @throws Exception
     */
    public function onOrderWrittenEvent(EntityWrittenEvent $event)
    {
        try {
            /** @var EntityWriteResult $result */
            $result = $event->getWriteResults()[0] ?? null;
            if(! $result || $result->getOperation() != EntityWriteResult::OPERATION_INSERT){
                return;
            }

            // Get the order ID
            $orderPayload = $event->getPayloads();
            $orderId = $orderPayload[0]['id'] ?? null;

            // If nothing, abort
            if(! $orderId){
                throw new Exception('Order ID not found in the payload.');
            }

            /** @var OrderEntity $order */
            $order = $this->orderDataLoader->loadFromId($orderId);

            if(! $order){

                throw new Exception("Order $orderId was not found in the system");
            }

            $this->synchronizerResolver->getSynchronizerDriverByName()->synchronize(new OrderCollection([$order]));

        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage());
            throw $exception;
        }
    }

    /**
     * @param OrderStateMachineStateChangeEvent $event
     * @throws Exception
     */
    public function onOrderStateChange(OrderStateMachineStateChangeEvent $event)
    {
        $orderId = $event->getOrder()->getId();

        /** @var OrderEntity $order */
        $order = $this->orderDataLoader->loadFromId($orderId);

        $this->synchronizerResolver->getSynchronizerDriverByName()->updateOrderPaymentStatus($order);
    }

}
