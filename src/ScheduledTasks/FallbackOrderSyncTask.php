<?php declare(strict_types=1);

namespace OrderSynchronizer\ScheduledTasks;

use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTask;


class FallbackOrderSyncTask extends ScheduledTask
{
    public static function getTaskName(): string
    {
        return 'neta-order-synchronizer.fallback-order-sync-task';
    }
    public static function getDefaultInterval(): int
    {
        return 86400; // 1 day
    }
}