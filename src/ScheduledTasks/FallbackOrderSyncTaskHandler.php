<?php declare(strict_types=1);

namespace OrderSynchronizer\ScheduledTasks;


use DateTime;
use DateTimeImmutable;
use Exception;
use OrderSynchronizer\DataLoaders\OrderDataLoader;
use OrderSynchronizer\Synchronizers\SynchronizerResolver;
use Shopware\Core\Checkout\Order\OrderCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskHandler;

class FallbackOrderSyncTaskHandler extends ScheduledTaskHandler
{
    /**
     * @var SynchronizerResolver
     */
    protected SynchronizerResolver $synchronizerResolver;
    /**
     * @var OrderDataLoader
     */
    protected OrderDataLoader $orderDataLoader;

    /**
     * @param EntityRepositoryInterface $scheduledTaskRepository
     * @param SynchronizerResolver $synchronizerResolver
     * @param OrderDataLoader $orderDataLoader
     */
    public function __construct(EntityRepositoryInterface $scheduledTaskRepository,
                                SynchronizerResolver $synchronizerResolver,
                                OrderDataLoader $orderDataLoader)
    {
        parent::__construct($scheduledTaskRepository);

        $this->synchronizerResolver = $synchronizerResolver;
        $this->orderDataLoader =$orderDataLoader;
    }

    public static function getHandledMessages(): iterable
    {
        return [ FallbackOrderSyncTask::class ];
    }

    /**
     * @throws Exception
     */
    public function run(): void
    {
        $from = DateTimeImmutable::createFromMutable(new DateTime('yesterday'));
        $to = new DateTimeImmutable((new DateTime('yesterday'))->format('Y-m-d 23:59:59'));

        /** @var OrderCollection $orders */
        $orders = $this->orderDataLoader->loadFromDateRange($from, $to);

        $this->synchronizerResolver->getSynchronizerDriverByName()->synchronize($orders);
    }
}