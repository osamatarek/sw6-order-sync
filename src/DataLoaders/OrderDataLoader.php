<?php namespace OrderSynchronizer\DataLoaders;

use DateTimeImmutable;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\RangeFilter;

class OrderDataLoader extends BaseDataLoader {

    protected EntityRepositoryInterface $orderRepository;

    public function __construct(EntityRepositoryInterface $orderRepository)
    {
        parent::__construct();

        $this->orderRepository = $orderRepository;
    }

    /**
     * @inheritDoc
     */
    public function loadFromId(string $resourceId) : ? Entity {

        return $this->search(new Criteria([$resourceId]))->first();
    }

    public function loadFromOrderNumber(string $orderNumber) : ? Entity
    {
        $criteria = new Criteria();

        $criteria->addFilter(
            new EqualsFilter("orderNumber", $orderNumber)
        );

        return $this->search($criteria)->first();
    }


    public function search(Criteria $criteria): ? EntitySearchResult
    {
        $criteria->addAssociations([
            'lineItems.product',
            'transactions.paymentMethod',
            'language.locale',
            'addresses.country',
            'deliveries.shippingOrderAddress.country',
        ]);

        // Return the first result
        return $this->orderRepository->search($criteria, $this->context);
    }

    public function loadFromDateRange(DateTimeImmutable $from, DateTimeImmutable $to): ? EntityCollection
    {
        $criteria = new Criteria();

        $criteria->addFilter(
            new RangeFilter('orderDateTime', [
                RangeFilter::GTE => $from->format("Y-m-d H:i:s"),
                RangeFilter::LTE => $to->format("Y-m-d H:i:s")
            ]),
        );

        return $this->orderRepository->search($criteria, $this->context)->getEntities();
    }
}
