<?php namespace OrderSynchronizer\DataLoaders;

use Shopware\Core\Framework\Context;

abstract class BaseDataLoader implements DataLoaderInterface{

    // Used for shared functionality between data loaders

    /** @var Context */
    protected Context $context;

    /**
     * BaseDataLoader constructor.
     */
    public function __construct()
    {
        $this->context = Context::createDefaultContext();
    }

}
