<?php namespace OrderSynchronizer\DataLoaders;

use DateTimeImmutable;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

interface DataLoaderInterface{

    /**
     * Loads a given resource by its id, and it's relationships
     *
     * @param string $resourceId
     *
     * @return Entity|null
     */
    public function loadFromId(string $resourceId) : ? Entity;

    /**
     * Loads a given resource by its id, and it's relationships
     *
     * @param string $orderNumber
     *
     * @return Entity|null
     */
    public function loadFromOrderNumber(string $orderNumber) : ? Entity;


    /**
     * Loads a collection of resources by the creation dates, and it's relationships
     *
     * @param \DateTimeImmutable $from
     * @param \DateTimeImmutable $to
     *
     * @return \Shopware\Core\Framework\DataAbstractionLayer\EntityCollection|null
     */
    public function loadFromDateRange(DateTimeImmutable $from, DateTimeImmutable $to) : ? EntityCollection;
}
