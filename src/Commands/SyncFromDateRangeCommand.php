<?php declare(strict_types=1);

namespace OrderSynchronizer\Commands;


use DateTime;
use DateTimeImmutable;
use Exception;
use OrderSynchronizer\DataLoaders\OrderDataLoader;
use OrderSynchronizer\Synchronizers\SynchronizerResolver;
use Shopware\Core\Checkout\Order\OrderCollection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncFromDateRangeCommand extends Command {

    protected SynchronizerResolver $synchronizerResolver;
    protected OrderDataLoader $orderDataLoader;

    /**
     * @param SynchronizerResolver $synchronizerResolver
     * @param OrderDataLoader $orderDataLoader
     *
     */
    public function __construct(SynchronizerResolver $synchronizerResolver, OrderDataLoader $orderDataLoader)
    {
        parent::__construct();

        $this->synchronizerResolver = $synchronizerResolver;
        $this->orderDataLoader =$orderDataLoader;
    }

    // Command name
    protected static string $defaultName = 'order-synchronizer:sync-from-date-range';

    // Provides a description, printed out in bin/console
    protected function configure(): void
    {
        $this->addArgument('from', InputArgument::REQUIRED, 'The start date to check orders to be synced. Please use format dd/mm/yyyy.');
        $this->addArgument('to', InputArgument::OPTIONAL, 'The end date to check orders to be synced. Please use format dd/mm/yyyy.', (new DateTime)->format("d/m/Y"));
        $this->setDescription('Manually synchronizes orders given a from and to dates');
    }


    /**
     * Actual code executed in the command
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $from = DateTimeImmutable::createFromFormat("d/m/Y H:i:s", $input->getArgument('from') . " 00:00:00");
        $to = DateTimeImmutable::createFromFormat("d/m/Y H:i:s", $input->getArgument('to') . " 23:59:59");

        if(! $from || ! $to){
            throw new Exception("Error while parsing the date range. Please check if your from and to parameters are using the format dd/mm/yyyy");
        }

        $output->writeln("Synchronizing orders from {$from->format('d/m/Y H:i:s')} to {$to->format('d/m/Y H:i:s')}", OutputInterface::VERBOSITY_NORMAL);

        /** @var OrderCollection $orders */
        $orders = $this->orderDataLoader->loadFromDateRange($from, $to);

        $this->synchronizerResolver->getSynchronizerDriverByName()->synchronize($orders);
        // Exit code 0 for success
        return 0;
    }

}