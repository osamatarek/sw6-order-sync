<?php declare(strict_types=1);

namespace OrderSynchronizer\Commands;


use Exception;
use OrderSynchronizer\DataLoaders\OrderDataLoader;
use OrderSynchronizer\Synchronizers\SynchronizerResolver;
use Shopware\Core\Checkout\Order\OrderCollection;
use Shopware\Core\Checkout\Order\OrderEntity;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncOneCommand extends Command {

    protected SynchronizerResolver $synchronizerResolver;
    protected OrderDataLoader $orderDataLoader;

    /**
     * @param SynchronizerResolver $synchronizerResolver
     * @param OrderDataLoader $orderDataLoader
     *
     */
    public function __construct(SynchronizerResolver $synchronizerResolver, OrderDataLoader $orderDataLoader)
    {
        parent::__construct();

        $this->synchronizerResolver = $synchronizerResolver;
        $this->orderDataLoader =$orderDataLoader;
    }

    // Command name
    protected static string $defaultName = 'order-synchronizer:sync-one';

    // Provides a description, printed out in bin/console
    protected function configure(): void
    {
        $this->addArgument('orderNumber', InputArgument::REQUIRED, 'The order number to be synced');
        $this->setDescription('Manually synchronizes an order');
    }


    /**
     * Actual code executed in the command
     *
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $orderNumber = $input->getArgument('orderNumber');
        $output->writeln("Synchronizing order $orderNumber", OutputInterface::VERBOSITY_NORMAL);

        /** @var OrderEntity $order */
        $order = $this->orderDataLoader->loadFromOrderNumber($orderNumber);

        if(! $order){
            throw new Exception("Order for order number $orderNumber not found.");
        }

        $this->synchronizerResolver->getSynchronizerDriverByName()->synchronize(new OrderCollection([$order]));
        // Exit code 0 for success
        return 0;
    }

}