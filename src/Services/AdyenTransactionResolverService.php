<?php declare(strict_types=1);

namespace OrderSynchronizer\Services;

use Exception;
use Shopware\Core\Checkout\Order\OrderEntity;
use Adyen\Shopware\Service\PaymentResponseService;

class AdyenTransactionResolverService implements TransactionResolverInterface {

    /** @var PaymentResponseService\ */
    protected $paymentResponseService;

    public function __construct(PaymentResponseService $paymentResponseService = null)
    {
        $this->paymentResponseService = $paymentResponseService;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function getTransactionId(OrderEntity $orderEntity): string
    {
        if(! $this->paymentResponseService){
            throw new Exception("Adyen plugin for Shopware 6 is not installed.");
        }

        $paymentResponse = $this->paymentResponseService->getWithOrderId($orderEntity->getId());

        if($paymentResponse){
            $paymentResponseArray = json_decode($paymentResponse->getResponse(), true);
            $pspReference = $paymentResponseArray['pspReference'] ?? "";
        } else {
            $pspReference = "";
        }

        return $pspReference;
    }

    public function getPluginKey() : string
    {
        return "Adyen"; // TODO: Make it dynamic
    }
}