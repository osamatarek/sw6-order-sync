<?php declare(strict_types=1);

namespace OrderSynchronizer\Services;

use Exception;
use Shopware\Core\Checkout\Order\OrderEntity;
use Sellxed\DatatransCw\Service\TransactionEntityService;

class DatatransTransactionResolverService implements TransactionResolverInterface {

    /** @var TransactionEntityService */
    protected $transactionEntityService;

    public function __construct(TransactionEntityService $transactionEntityService = null)
    {
        $this->transactionEntityService = $transactionEntityService;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function getTransactionId(OrderEntity $orderEntity): string
    {
        if(! $this->transactionEntityService){
            throw new Exception("Datatrans plugin for Shopware 6 is not installed.");
        }

        $paymentResponse = $this->transactionEntityService->getTransactionByOrderId($orderEntity->getId());

        return  $paymentResponse->getShopwareOrderTransactionId();

    }

    public function getPluginKey() : string
    {
        return "Datatrans"; // TODO: Make it dynamic
    }
}