<?php declare(strict_types=1);

namespace OrderSynchronizer\Services;

use Shopware\Core\Checkout\Order\OrderEntity;

interface TransactionResolverInterface{

    /**
     * @param OrderEntity $orderEntity
     *
     * @return string
     */
    public function getTransactionId(OrderEntity $orderEntity) : string;

    /**
     * @return string
     */
    public function getPluginKey() : string;

}