# Shopware Order Synchronizer

Synchronize shop orders between Shopware6 and Odoo API

## Configuration 
Update .env  
`ODOO_SHOPWARE_API_URL=`
`ODOO_SHOPWARE_API_CLIENT_ID=`
`ODOO_SHOPWARE_API_CLIENT_SECRET=`

## TODO
- [ ] queue tasks
- [ ] tests
- [ ] update README
